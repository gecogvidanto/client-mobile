/*
 * This file is part of @gecogvidanto/client-mobile.
 *
 * @gecogvidanto/client-mobile is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @gecogvidanto/client-mobile is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with @gecogvidanto/client-mobile. If not, see <http://www.gnu.org/licenses/>.
 */

import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
})

export default class App extends Component<{}> {
  public render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Ici prochainement</Text>
        <Text style={styles.instructions}>
          Une magnifique application pour aider à l'animation du Ğeconomicus.
        </Text>
      </View>
    )
  }
}
