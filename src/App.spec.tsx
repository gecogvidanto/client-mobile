/*
 * This file is part of @gecogvidanto/client-mobile.
 *
 * @gecogvidanto/client-mobile is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @gecogvidanto/client-mobile is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with @gecogvidanto/client-mobile. If not, see <http://www.gnu.org/licenses/>.
 */

// tslint:disable:no-implicit-dependencies (dev dependencies are enough for tests)
// tslint:disable:only-arrow-functions (mocha discourage to use arrow function)
// tslint:disable:no-unused-expression (chai expressions are indeed side-effect)
import React from 'react'
import 'react-native'
import App from './App'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

describe('<App />', function() {
  it('renders correctly', () => {
    renderer.create(<App />)
  })
})
