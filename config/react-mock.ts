/*
 * This file is part of @gecogvidanto/client-mobile.
 *
 * @gecogvidanto/client-mobile is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @gecogvidanto/client-mobile is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with @gecogvidanto/client-mobile. If not, see <http://www.gnu.org/licenses/>.
 */

// tslint:disable:no-implicit-dependencies (dev dependencies are enough for configuration)
import createClass from 'create-react-class'
import PropTypes from 'prop-types'
import React from 'react'

import StyleSheet from './StyleSheet'

const componentsToMock = ['Text', 'View']

function mockComponent(type: string): React.ComponentClass<any> {
  return createClass({
    displayName: type,
    propTypes: { children: PropTypes.node },
    render() {
      return React.createElement('div', this.props, this.props.children)
    },
  })
}

const mockComponents = componentsToMock.reduce(
  (agg, type) => {
    agg[type] = mockComponent(type)
    return agg
  },
  {
    StyleSheet,
  } as any
)

// the cache key that real react native would get
const key = require.resolve('react-native')

// make sure the cache is filled with our lib
require.cache[key] = {
  id: key,
  filename: key,
  loaded: true,
  exports: mockComponents,
}
