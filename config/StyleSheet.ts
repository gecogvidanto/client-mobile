/*
 * This file is part of @gecogvidanto/client-mobile.
 *
 * @gecogvidanto/client-mobile is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @gecogvidanto/client-mobile is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with @gecogvidanto/client-mobile. If not, see <http://www.gnu.org/licenses/>.
 */

// tslint:disable-next-line:variable-name
const StyleSheet = {
  create(styles: any): any {
    return styles
  },
  flatten(styles: any): any {
    if (Array.isArray(styles)) {
      return Object.assign({}, ...styles.map(StyleSheet.flatten))
    }
    return styles
  },
}

export default StyleSheet
